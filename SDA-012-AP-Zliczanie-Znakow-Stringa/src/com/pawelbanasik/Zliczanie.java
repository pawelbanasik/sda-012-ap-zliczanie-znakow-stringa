package com.pawelbanasik;

import java.util.Scanner;

public class Zliczanie {

	public static void main(String[] args) {

		String text = "Ala ma kota";

		Scanner sc = new Scanner(System.in);

		char znak = sc.next().charAt(0);

		char[] znaki = text.toCharArray();

		int ilosc = 0;

		for (int i = 0; i < znaki.length; i++) {

			if (Character.toLowerCase(znaki[i]) == Character.toLowerCase(znak)) {
				ilosc++;

			}

		}
		System.out.println(ilosc);
	}
}
